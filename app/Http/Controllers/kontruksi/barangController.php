<?php

namespace App\Http\Controllers\kontruksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class barangController extends Controller{

    public function index(){
        return view('kontruksi.barang.index');
    }

    public function create(){
        return view('kontruksi.barang.create');
    }

    public function store(Request $request){
        
    }

    public function show($id){
        
    }

    public function edit($id){
        
    }

    public function update(Request $request, $id){
        
    }

    public function destroy($id){
        
    }
}
